var config = require('../Config/Config');
var moment = require('moment');

function calculateOffset(page) {
	var offsetRow = (parseInt(page) - 1) * config.env.rowsPerPage;
	return offsetRow;
}

function reformatFileArrivedTime(data) {
	for(var i = 0; i < data.length; i++) {
		data[i].FILEARRIVEDTIME = moment(data[i].FILEARRIVEDTIME).tz("Etc/GMT").format('dddd, MMMM Do YYYY, h:mm:ss a');
	}
	return data;
}

module.exports.calculateOffset = calculateOffset;
module.exports.reformatFileArrivedTime = reformatFileArrivedTime;