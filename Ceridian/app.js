var express = require('express');
var handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
var passport = require("passport");
var path = require('path');
var fs = require('fs');
var session = require('express-session');
//var SequelizeStore = require('connect-session-sequelize')(session.Store);
var env = require('dotenv').load();
var flash = require('connect-flash');
var expressflash = require('express-flash');
var expressmessage = require('express-messages');

var app = express();
var models = require("./models");

/*Flash Messaging*/
app.use(require('connect-flash')());
app.use(function(req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});

//handlebars
app.engine('handlebars', handlebars());

app.set('view engine', 'handlebars');

//Set Static Folder
app.use(express.static(path.join(__dirname, '/public')));
//express-sessions

/*var myStore = new SequelizeStore({
    db: models.localDB.sequelize
})*/
app.use(session({
    secret: 'Ceridian',
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false }
}));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());

//BodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Sync Database
/*models.localDB.sequelize.sync().then(function() {
 
    console.log('Nice! Database looks fine')
 
}).catch(function(err) {
 
    console.log(err, "Something went wrong with the Database Update!")
 
});*/


app.use('/', require('./routes/index.js'));

app.listen(8000);