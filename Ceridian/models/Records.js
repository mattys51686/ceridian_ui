module.exports = function(sequelize,Sequelize){
	
	var Records = sequelize.define('CER_TRACKER_CF', {
		INDSEG: {
			type: Sequelize.STRING,
			primaryKey:true,
			allowNull: false
		},

		FORMAT: {
			type: Sequelize.STRING,
			allowNull: false
		},

		VERSION:{
			type: Sequelize.STRING,
			allowNull: false
		},

		ORGTYPE:{
			type: Sequelize.STRING,
			allowNull: false
		},

		ORGTVAL: {
			type: Sequelize.STRING,
			allowNull: false
		},

		ORGGRP: {
			type: Sequelize.STRING,
			allowNull: true
		},

		ORGGVAL: {
			type: Sequelize.STRING,
			allowNull: true
		},

		YEAR: {
			type: Sequelize.STRING,
			allowNull: true
		},
		
		PERIOD: {
			type: Sequelize.STRING,
			allowNull: true
		},
		
		SUFFIX: {
			type: Sequelize.STRING,
			allowNull: true
		},

		PROCID: {
			type: Sequelize.STRING,
			allowNull: true
		},
		
		CTCRECIP: {
			type: Sequelize.STRING,
			allowNull: true
		},
		
		DEST: {
			type: Sequelize.STRING,
			allowNull: false
		},	
		
		EFFDATE: {
			type: Sequelize.STRING,
			allowNull: false
		},
	
		CHKDATE: {
			type: Sequelize.STRING,
			allowNull: false
		},
	
		CRDATE: {
			type: Sequelize.STRING,
			allowNull: true
		},	
	
		FILETYPE: {
			type: Sequelize.STRING,
			allowNull: false
		},

		ACCTNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},

		ROUTNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},

		DBTRNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},

		DBTACCT: {
			type: Sequelize.STRING,
			allowNull: true
		},		
		DATATYPE: {
			type: Sequelize.STRING,
			allowNull: true
		},						

		VALUE1: {
			type: Sequelize.STRING,
			allowNull: true
		},

		VALUE2: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		ACCTNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},

		DOLLARAMT: {
			type: Sequelize.STRING,
			allowNull: true
		},

		DOLLARAMT2: {
			type: Sequelize.STRING,
			allowNull: true
		},

		IBECKNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		IENCKNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		VBECKNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		VENCKNUM: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		TOTDETAIL: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		ORIGFILEID: {
			type: Sequelize.STRING,
			allowNull: true
		},	

		ORIGFILENAME: {
			type: Sequelize.STRING,
			allowNull: true
		},

		BATCHID: {
			type: Sequelize.STRING,
			allowNull: true
		},

		STATUS: {
			type: Sequelize.STRING,
			allowNull: true
		},
		
		COMMENTS: {
			type: Sequelize.STRING,
			allowNull: true
		},

		SFGERRORCODE: {
			type: Sequelize.STRING,
			allowNull: true
		},
				
		SFGERRORDESC: {
			type: Sequelize.STRING,
			allowNull: true
		},

		FILEARRIVEDTIME: {
			type: Sequelize.STRING,
			allowNull: true
		},
										
	},{
		raw:true,
		timestamps:false,
		freezeTableName: true
	});
	
	return Records;

}