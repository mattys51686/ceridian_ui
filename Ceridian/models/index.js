"use strict";
 
var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require('../Config/Config');

var conn = new Sequelize(config.env.database,config.env.username, config.env.password, {
    host:config.env.host,
    dialect:config.env.dialect,
    logging:false
});

var db = {};


var recordsModel = conn.import(path.join(__dirname,"Records.js"));
db[recordsModel.name] = recordsModel;


db.sequelize = conn;
db.Sequelize = Sequelize;


module.exports.db = db;