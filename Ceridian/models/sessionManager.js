var Sessions = {};


function createSession(username,password){
	var session = {
		username:username,
		password:password
	}
	Sessions[username] = session;
}


function getSession(username){
	return Sessions[username];
}

function removeSession(username){
	delete Sessions[username];
}


module.exports.Sessions = Sessions;
module.exports.createSession = createSession;
module.exports.getSession = getSession;
module.exports.removeSession = removeSession;