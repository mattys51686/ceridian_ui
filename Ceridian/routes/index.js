
var express = require('express');
var async = require("async");
var passport = require("passport");
var LocalStrategy = require('passport-local').Strategy;
var parseString = require("xml2js").parseString;
var request = require("request");
var async = require("async");
var flash = require('connect-flash');

var router = express.Router();
var sessionManager = require("../models/sessionManager");
var config = require('../Config/Config');


// Router Get
router.get('/',function(req, res){
    if(req.isAuthenticated())
    {
      res.redirect('/user/Dashboard');
    }else{
      res.render("login", {env:config.env.envName,error: req.flash('error')});
    }
    
});


router.post('/login',passport.authenticate('local',{
  successRedirect:'/user/Dashboard',
  failureRedirect: '/',
  failureFlash: true 
}));

// Passport
passport.use('local',new LocalStrategy({
	  usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
},
  function(req,username, password, done) {
      var loginURL = config.env.envURL + "login";
  		request.post(loginURL,{'auth':{
  			'username':username,
  			'password':password,
  			'sendImmediately':true}},
  		function(error,response,body){
  			 //if(error) return done(null, false, {message:'The Username/Password Entered is Incorrect. Please Try Again!'});
         async.waterfall([
              function(callback){
                parseString(response.body,function(err,result){
                 // console.log(result);
                  if(!result.UIusername)
                    return done(null, false, { message: 'The Username/Password Entered is Incorrect. Please Try Again!'});
                    callback(err,result.UIusername.trim());
                });
              },
              function(user,callback){
                sessionManager.createSession(user,password);
                done(null,user);
              }

          ]);
      });
  }));

  passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
  passport.deserializeUser(function(id, done) {
    done(null,sessionManager.getSession(id));
  });

//Infinitely loops .....
  router.use('/user',function(req,res,next) {
    if(req.isAuthenticated())
    {
      return next();
    }else{
      res.redirect('/');
    }
  });

router.use('/user', require('./user.js'));
module.exports = router;