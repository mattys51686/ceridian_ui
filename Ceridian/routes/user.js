var express = require('express');
var request = require('request');
var builder = require('xmlbuilder');
var parseString = require('xml2js').parseString;
var pagination = require('pagination');
var database = require('../models').db;
var sessionManager = require('../models/sessionManager');
var config = require('../Config/Config');
var Sequelize = require('sequelize');
var calculate = require('../functions/functions');
const Op = Sequelize.Op;
var moment = require('moment');

var router = express.Router();

router.get('/Dashboard', function(req, res, next) {
    var query = {};
    const where = {
        [Op.and]: query
    };
    var useWhere;

    let { recipient, indseg, status, startDate, endDate, page = 1 } = req.query;

    if (!recipient && !indseg && !status && !startDate && !endDate) {
        useWhere = false;
    } else {
        if (recipient) {
            query.CTCRECIP = recipient;
        }

        if (indseg) {
            query.INDSEG = indseg;
        }

        if (status) {
            query.STATUS = status;
        }

        if (startDate && endDate) {
            startDate = moment(startDate, 'YYYY-MM-DD', true).add(-1, 'day').tz('Etc/GMT').toDate();
            endDate = moment(endDate, 'YYYY-MM-DD', true).tz('Etc/GMT').toDate();
            query.EFFDATE = {
                [Op.between]: [startDate, endDate]
            }
        }

        useWhere = true;
    }

    var skippedRows = calculate.calculateOffset(page);
    database.CER_TRACKER_CF.findAndCountAll({
        where: useWhere ? where : undefined,
        offset: skippedRows,
        limit: config.env.rowsPerPage,
    })

    .then(function(tableData) {
        count = parseInt(tableData.count);
        var paginator = new pagination.SearchPaginator({ prelink: '/', current: page, rowsPerPage: config.env.rowsPerPage, totalResult: count });
        var info = paginator.getPaginationData();
        calculate.reformatFileArrivedTime(tableData);
        // res.render('dashboard',{tableData, info});
        res.json({ tableData, info });
    })

    .catch(function(err) {
        return req.flash('error', 'No Active Database Connect. Please Try Again at a Later Time!');
    })
});

// router.get('/test', function (req, res, next) {
//   let {recipient, indseg, status, startDate, endDate, page = 1} = req.query;
//   var query={};

//   if (recipient || indseg || status || startDate || endDate) {

//     if (recipient) {
//       query.CTCRECIP = recipient;
//     }

//     if (indseg) {
//       query.INDSEG = indseg;
//     }

//     if (status) {
//       query.STATUS = status;
//     }

//     if (startDate && endDate) {
//       startDate = moment(startDate,'YYYY-MM-DD').tz("Etc/GMT").toDate();
//       endDate = moment(endDate,'YYYY-MM-DD').tz("Etc/GMT").toDate();
//       query.EFFDATE = {
//         [Op.between]: [startDate,endDate]
//       }
//     }
//   } else {
//     return req.flash('error','Require at least 1 query param');
//   }

//   var skippedRows = calculate.calculateOffset(page);
//   database.CER_TRACKER_CF.findAndCountAll({
//     where: {
//       [Op.and]: query
//     },
//     offset: skippedRows, 
//     limit: config.env.rowsPerPage, 
//   })
//   .then(function(filteredResults) {
//     count = parseInt(filteredResults.count);
//     var paginator = new pagination.SearchPaginator({prelink:'/', current: page, rowsPerPage: config.env.rowsPerPage, totalResult: count});
//     var data = paginator.getPaginationData(); 
//     res.json({filteredResults, data});
//   })
//   .catch(function(err) {
//     return req.flash('error','No Active Database Connect. Please Try Again at a Later Time!');
//   })
// });

/*
router.get('/Dashboard',function(req, res){
  async.waterfall([
    function(callback){
      database.CER_TRACKER_CF.findAll({raw:true}).catch(function(err){
        return req.flash('error','No Active Database Connect. Please Try Again at a Later Time!');
      }).then(function(result){
      for(var i = 0; i < result.length; i++)
      {
        result[i].FILEARRIVEDTIME = moment(result[i].FILEARRIVEDTIME).tz("Etc/GMT").format('dddd, MMMM Do YYYY, h:mm:ss a');
      }
      callback(null,result);
      })
    },

    function(tabledata,callback)
    {
      database.CER_TRACKER_CF.aggregate('CTCRECIP', 'DISTINCT', { plain: false, raw:true }).catch(function(err){
          return req.flash('error','No Active Database Connect. Please Try Again at a Later Time!');
        }).then(function(result){
         callback(null,tabledata,result);
      })
    },
    
    function(CTCRECIP,tabledata,callback)
    {
      database.CER_TRACKER_CF.aggregate('INDSEG', 'DISTINCT', { plain: false, raw:true }).catch(function(err){
          return req.flash('error','No Active Database Connect. Please Try Again at a Later Time!');
        }).then(function(result){
        callback(null,CTCRECIP,tabledata,result);
      })
    }
  ],

    function(err,table,row,ind){
        res.render('dashboard',{tableData:table,CTCRECIP:row,env:config.env.envName,INDSEG:ind});
    });

});
*/

router.post('/Action/:ActionType', function(req, res) {
    //console.log("POST was called!!");
    //console.log(req.body[req.body.length-1].COMMENT);
    var xml = builder.create('FILEIDS');
    xml.ele('COMMENTS', req.body.COMMENT).end();
    if (Array.isArray(req.body.FILEID)) {
        for (var i = 0; i < req.body.FILEID.length; i++) {
            xml.ele('FILEID', req.body.FILEID[i]).end();
        }
    } else {
        xml.ele('FILEID', req.body.FILEID).end();
    }

    xml.end({ pretty: true });
    var postURL = config.env.envURL + req.params.ActionType;
    request.post(postURL, {
            'auth': {
                'username': req.user.username,
                'password': req.user.password,
                'sendImmediately': true
            },
            'body': xml.toString()
        },
        function(error, response, body) {
            //flash messaging goes here
            //  Need to call flash messaging not res.send(error)
            //console.log(body);

            //res.redirect('/user/Dashboard');
            parseString(body, function(err, result) {
                //console.log(result);
                if (result.response.$.errormessage) {
                    //  req.flash('error',result.response.$.errormessage);
                    // console.log("got to error");
                    //  res.redirect('/user/Dashboard');
                    var response = result.response.$.errormessage;
                    database.CER_TRACKER_CF.findAll({ raw: true }).then(function(result) {
                        res.render('dashboard', { tableData: result, env: config.env.envName, error: response });
                    });
                    //res.send(result.response.$.errormessage);

                } else {
                    database.CER_TRACKER_CF.findAll({ raw: true }).then(function(result) {
                        res.render('dashboard', { tableData: result, env: config.env.envName, success: 'Action was submitted successfully!' });
                    });
                }

            });
        });

    //console.log(xml.toString());
});

router.get('/logout', function(req, res) {
    req.session.destroy();
    //console.log(sessionManager.Sessions);
    sessionManager.removeSession(req.user.username);
    //console.log(sessionManager.Sessions);
    req.logout();
    res.redirect('/');
});

module.exports = router;