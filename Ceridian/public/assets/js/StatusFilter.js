filterActive = false;
activeStatus = [];
assetTable = document.getElementById("myTable");
tableRows = undefined;


//********************EVENT HANDLERS********************//

// Check for Checked Boxes & Filter Based on Selected Status
function rowCheckboxHandler(it) {
  tr = it.parentElement.parentElement;
  if (it.checked) {
    activeStatus.push(serializer(tr));
    //document.getElementById('selectedRows').value = activeStatus;
    tr.style.backgroundColor = "#ffc166";

    if (activeStatus.length == 1 && !filterActive) {
      filter = getRowStatus(tr).innerHTML.toUpperCase();
      // filterByStatus(assetTable, filter);
      applySimpleFilter(assetTable, filter, rowAttr.status);
      filterActive = true;
      //console.log(assetTable);
      actionSelection(filter);
    }     
  } 
  else {
    
    activeStatus.pop();
    tr.style.backgroundColor = '#EFEBE9';
    filterActive = false;
    filter = null;
    if(activeStatus.length == 0){
    if (filter == null) {
            $("#actions").html("<option disabled selected value>No Row Selected</option>");
                    
        }  
    if (activeStatus.length == 0 && !filterActive) {
      displayAllRows(document.getElementById("myTable"));
    }
  }
 }
}
// Action Selection
function actionSelection(){
      if(filter == 'HOLD'){
          $("#actions").html("<option disabled selected value>Select an Action</option> <option value='CER_Operator_TerminateFromHold'>Terminate from Hold</option> <option value='CER_Operator_ReleaseFromHold'>Release from Hold</option> ");
          document.getElementById('openM').disabled = this.checked;
      }
      else if (filter == "TERMINATED") {
            $("#actions").html("<option disabled selected value>No Action Available</option>");
        }
      else if (filter == "RECEIVED") {
            $("#actions").html("<option disabled selected value>No Action Available</option>");
        }
      else if (filter == "MARKED FOR DELIVERY") {
            $("#actions").html("<option disabled selected value>No Action Available</option>");
        } 
      else if (filter == "FAILED") {
            $("#actions").html("<option disabled selected value>No Action Available</option>");
        }
      else if (filter == "EMPTY") {
            $("#actions").html("<option disabled selected value>No Action Available</option>");
        }
      else if (filter == "WAREHOUSE") {
            $("#actions").html("<option disabled selected value>Select an Action</option><option value='CER_Operator_MoveWarehouseToHold'>Move Warehouse to Hold</option><option value='CER_Operator_TerminateFromWarehouse'>Terminate from Warehouse</option>");
            document.getElementById('openM').disabled = this.checked;
        }                 
}

// Input Filters
function getRowStatus(row) {
  return row.getElementsByTagName("td")[6];
}

function datePickerChange() {
  minDate = document.getElementById("minDatePicker").value;
  maxDate = document.getElementById("maxDatePicker").value;
  //console.log(rowAttr.checkDate);

  if (minDate != "" && maxDate != "") {    
    applySimpleRangeFilter(assetTable, minDate, maxDate,rowAttr.checkDate);
  }
}

//********************Generic Filter Helpers********************//

rowAttr = { 
  status: 6, 
  recipient: 1,
  industry: 2,
  checkDate: 4,
  getAttr: function(row, attrID) {
    return row.getElementsByTagName("td")[attrID];
  } };

evalAttr = {
  simple: function(val, target) {
    //console.log(val.innerHTML.toUpperCase());
    return val.innerHTML.toUpperCase().indexOf(target) > -1;
  },
  // Need to fix this Simple Range 
  simpleRange: function(val, min, max) {
    
   
    val = val.innerHTML;
    val = Date.parse(val);
    min = Date.parse(min);
    max = Date.parse(max);
    return val >= min && val <= max;
  },
}

function applySimpleFilter(tbl, filterValue, attrID) {
  if (tableRows == undefined) {
    tableRows = tbl.getElementsByTagName("tr");
  }
  simpleFilter = function(val) {
    return evalAttr.simple(val, filterValue);
  }


  for (i = 0; i < tableRows.length; i++) {
    toggleRowByFilter(tableRows[i], simpleFilter, attrID);
  }
}

function applySimpleRangeFilter(tbl, minVal, maxVal, attrID) {
  rows = tbl.getElementsByTagName("tr");

  simpleRangeFilter = function(val) {
    return evalAttr.simpleRange(val, minVal, maxVal);
  }

  for (i = 0; i < rows.length; i++) {
    toggleRowByFilter(rows[i], simpleRangeFilter, attrID);
  }
}

function toggleRowByFilter(row, filterEvaluator, attrID) {
  if (td = rowAttr.getAttr(row, attrID)) {
    if (td) {
      if (filterEvaluator(td)) {
      
         row.style.display = "";
      }
      else {
       
        row.style.display = "none";
      }
    }
  }
}

function displayAllRows(tbl) {
  rows = tbl.getElementsByTagName("tr");
  for (i = 0; i < rows.length; i++) {
    rows[i].style.display = "";
  }
}

//********************Helpers********************//

function serializer(tr){
  var rowItems= tr.getElementsByTagName("td");
    return {
      "STATUS": rowItems[6].innerHTML, 
      "ORIGFILEID": rowItems[28].innerHTML
    }
}